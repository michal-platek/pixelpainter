﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace PixelPainter
{
    public class MainViewModel : ViewModelBase
    {
        private const string LinePattern = "^([^\\s]+)(\\s+([\\d]+)\\s+(\\d+))+\\s*$";
        private string outputDirectory = AppDomain.CurrentDomain.BaseDirectory;
        private Regex regex = new Regex(LinePattern);

        private string filePath = string.Empty;
        private string log;

        private ICommand loadFileCommand;
        private ICommand processFileCommand;

        public MainViewModel()
        {
            Set(nameof(LoadFileCommand), ref loadFileCommand, new RelayCommand(() => LoadFile()));
            Set(nameof(ProcessFileCommand), ref processFileCommand, new RelayCommand(() => ProcessFile()));
            AppendLogMessage($"Output path: {outputDirectory}");
            AppendLogMessage($"Output files prefix: \"processed_\"");
        }
        
        public string Log
        {
            get { return this.log; }
            set { this.log = value; }
        }

        public ICommand LoadFileCommand
        {
            get { return this.loadFileCommand; }
            set { this.loadFileCommand = value; }
        }

        public ICommand ProcessFileCommand
        {
            get { return this.processFileCommand; }
            set { this.processFileCommand = value; }
        }

        private void ProcessFile()
        {
            try
            {
                if (!string.IsNullOrEmpty(filePath))
                {
                    string[] contents = File.ReadAllLines(filePath);
                    int lineNumber = 1;
                    int totalLines = contents.Length;

                    foreach (string line in contents)
                    {
                        AppendLogMessage($"Processing line [{lineNumber}/{totalLines}]... ");
                        var match = regex.Match(line);
                        if (match.Success)
                        {
                            string imageFileName = match.Groups[1].Captures[0].Value;
                            var xCoords = match.Groups[3].Captures.Cast<Capture>().Select(c => int.Parse(c.Value) - 1).ToList();
                            var yCoords = match.Groups[4].Captures.Cast<Capture>().Select(c => int.Parse(c.Value) - 1).ToList();
                            var points = xCoords.Select((x, i) => new Point(x, yCoords[i]));

                            try
                            {
                                ProcessImage(imageFileName, points);
                                AppendLogMessage("Done.");
                            }
                            catch (Exception ex)
                            {
                                AppendLogMessage($"Error: {ex.Message}");
                            }
                        }
                        else
                        {
                            AppendLogMessage($"Warning: Bad syntax in line {lineNumber}: \"{line}\"");
                        }
                        lineNumber++;
                    }
                }
                else
                {
                    AppendLogMessage("Select an input file.");
                }
            }
            catch (Exception ex)
            {
                AppendLogMessage($"Error: {ex.Message}");
            }
        }

        private void ProcessImage(string imagePath, IEnumerable<Point> points)
        {
            Bitmap bitmap = null;
            using (var stream = File.OpenRead(imagePath))
            {
                bitmap = (Bitmap)Bitmap.FromStream(stream);
            }

            using (bitmap)
            using (var graphics = Graphics.FromImage(bitmap))
            {
                foreach(var point in points)
                {
                    graphics.FillRectangle(new SolidBrush(Color.Red), point.X, point.Y, 1, 1);
                }

                string outputFilePath = Path.Combine(outputDirectory, $"processed_{Path.GetFileNameWithoutExtension(imagePath)}.png");
                bitmap.Save(outputFilePath, ImageFormat.Png);
            }
        }

        private void LoadFile()
        {
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {
                filePath = dialog.FileName;
                AppendLogMessage($"Selected input file: {filePath}");
            }
        }

        private void AppendLogMessage(string message)
        {
            var stringBuilder = new StringBuilder(Log);
            stringBuilder.AppendLine(message);
            Set(nameof(Log), ref log, stringBuilder.ToString());
        }
    }
}