﻿using System.Windows;

namespace PixelPainter
{
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            MainViewModel viewModel = new MainViewModel();
            MainWindow view = new MainWindow(viewModel);
            view.Show();
        }
    }
}
