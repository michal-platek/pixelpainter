﻿using System.Windows;
using System.Windows.Controls;

namespace PixelPainter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainViewModel viewModel;
        
        public MainWindow(MainViewModel viewModel)
        {
            this.viewModel = viewModel;
            this.DataContext = viewModel;
            InitializeComponent();
        }

        public void LogBox_OnTextChanged(object sender, TextChangedEventArgs args)
        {
            TextBox textBox = sender as TextBox;
            if(textBox != null)
            {
                textBox.ScrollToEnd();
            }
            else
            {
                MessageBox.Show("Failed to scroll!");
            }
        }
    }
}
